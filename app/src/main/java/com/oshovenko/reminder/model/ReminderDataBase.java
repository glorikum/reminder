package com.oshovenko.reminder.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.oshovenko.reminder.database.DataBaseHelper;
import com.oshovenko.reminder.database.ViewData;

public class ReminderDataBase {
    private DataBaseHelper dataBaseHelper;

    public ReminderDataBase(DataBaseHelper dataBaseHelper) {
        this.dataBaseHelper = dataBaseHelper;
    }


    public void loadData(ViewData viewData) {
        SQLiteDatabase database = dataBaseHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("text", viewData.getText());
        contentValues.put("time", viewData.getTime());

        database.update(dataBaseHelper.getTableName(), contentValues, "id = ?",new String[] { "1" });
        dataBaseHelper.close();
    }

    public ViewData extractData() {
        ViewData viewData = new ViewData();
        SQLiteDatabase liteDatabase = dataBaseHelper.getWritableDatabase();
        Cursor cursor = liteDatabase.query(dataBaseHelper.getTableName(), null, null, null, null, null, null);
        cursor.moveToFirst();
        viewData.setText(cursor.getString(cursor.getColumnIndex("text")));
        viewData.setTime(cursor.getString(cursor.getColumnIndex("time")));
        cursor.close();
        dataBaseHelper.close();
        return viewData;
    }
}
