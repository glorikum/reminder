package com.oshovenko.reminder.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.oshovenko.reminder.database.DataBaseHelper;
import com.oshovenko.reminder.model.ReminderDataBase;
import com.oshovenko.reminder.database.ViewData;
import com.oshovenko.reminder.services.ReminderService;

public class ReminderReceiver extends BroadcastReceiver {
    private final String BOOT_ACTION = "android.intent.action.BOOT_COMPLETED";


    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equalsIgnoreCase(BOOT_ACTION)) {
            DataBaseHelper dataBaseHelper = new DataBaseHelper(context.getApplicationContext());
            ReminderDataBase model = new ReminderDataBase(dataBaseHelper);
            Intent newIntent = new Intent(context, ReminderService.class);
            ViewData viewData = model.extractData();
            newIntent.putExtra("text",viewData.getText());
            newIntent.putExtra("time",viewData.getTime());
            context.startService(newIntent);
        }
    }
}
