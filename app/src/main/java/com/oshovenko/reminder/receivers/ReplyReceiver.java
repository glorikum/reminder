package com.oshovenko.reminder.receivers;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.RemoteInput;

import com.oshovenko.reminder.R;
import com.oshovenko.reminder.constants.UntilConstants;
import com.oshovenko.reminder.database.DataBaseHelper;
import com.oshovenko.reminder.database.ViewData;
import com.oshovenko.reminder.model.ReminderDataBase;
import com.oshovenko.reminder.view.ReminderActivity;

public class ReplyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);

        if (remoteInput != null) {
            CharSequence replyText = remoteInput.getCharSequence("key_text_reply");
            if (replyText.toString().length() >= UntilConstants.CORRECT_TEXT_LENGTH){

                // Delete notification
                NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
                notificationManager.cancel(UntilConstants.NOTIFY_ID);

                //Updating the database
                overwriteDataBase(context, replyText);

                intent.setClass(context, ReminderActivity.class);
                context.startActivity(intent);
            }
        }
    }

    private void overwriteDataBase(Context context, CharSequence replyText) {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        ReminderDataBase reminderDataBase = new ReminderDataBase(dataBaseHelper);
        ViewData viewData = reminderDataBase.extractData();

        viewData.setText((String) replyText);
        reminderDataBase.loadData(viewData);
    }
}