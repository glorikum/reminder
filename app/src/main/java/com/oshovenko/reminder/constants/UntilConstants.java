package com.oshovenko.reminder.constants;

public interface UntilConstants {
     int CORRECT_TEXT_LENGTH = 3;

     String SERVICE_NAME = "com.oshovenko.reminder.services.ReminderService";

     int MIN = 1000;

     int NOTIFY_ID = 101;

     String SERVICE_CHANNEL_ID = "Reminder chanel";
}
