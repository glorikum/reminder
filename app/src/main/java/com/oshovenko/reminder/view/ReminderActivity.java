package com.oshovenko.reminder.view;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.oshovenko.reminder.R;
import com.oshovenko.reminder.database.DataBaseHelper;
import com.oshovenko.reminder.database.ViewData;
import com.oshovenko.reminder.model.ReminderDataBase;
import com.oshovenko.reminder.presenter.ReminderPresenter;



public class ReminderActivity extends AppCompatActivity {

    private ReminderPresenter presenter;
    private TextView textReminder;
    private TextView timeReminder;
    private Button buttonStart;
    private Button buttonStop;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        textReminder = findViewById(R.id.textInputField);
        timeReminder = findViewById(R.id.timeInputField);
        buttonStart = findViewById(R.id.buttonStart);
        buttonStop = findViewById(R.id.buttonStop);

        DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
        ReminderDataBase reminderDataBase = new ReminderDataBase(dataBaseHelper);
        presenter = new ReminderPresenter(reminderDataBase);
        presenter.attachView(this);
    }

    public void onClickButton(View view) {
        switch (view.getId()) {
            case R.id.buttonStart:
                presenter.startService();
                setButtonEnable(true);
                break;
            case R.id.buttonStop:
                presenter.stopService();
                setButtonEnable(false);
                break;
        }
    }

    public ViewData getValue(){
        ViewData viewData = new ViewData();
        viewData.setText(textReminder.getText().toString());
        viewData.setTime(timeReminder.getText().toString());
        return viewData;
    }

    public void setValue(ViewData viewData){
        textReminder.setText(viewData.getText());
        timeReminder.setText(viewData.getTime());
    }

    public void setTextError(){
        textReminder.setError(getString(R.string.valid_text));
    }

    public void setTimeError(){
        timeReminder.setError(getString(R.string.valid_time));
    }

    public void setButtonEnable(Boolean isServiceRun){
        buttonStart.setEnabled(!isServiceRun);
        buttonStop.setEnabled(isServiceRun);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.setPreviousValues();
        presenter.updateService();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }
}
