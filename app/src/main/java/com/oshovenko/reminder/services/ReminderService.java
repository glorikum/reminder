package com.oshovenko.reminder.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.RemoteInput;
import com.oshovenko.reminder.R;
import com.oshovenko.reminder.constants.UntilConstants;
import com.oshovenko.reminder.database.DataBaseHelper;
import com.oshovenko.reminder.database.ViewData;
import com.oshovenko.reminder.model.ReminderDataBase;
import com.oshovenko.reminder.receivers.ReplyReceiver;
import com.oshovenko.reminder.view.ReminderActivity;
import java.util.Timer;
import java.util.TimerTask;


public class ReminderService extends Service {
    String text;
    Timer timer;

    @Override
    public void onCreate(){
        super.onCreate();
        startForeground (102, new Notification());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(getApplicationContext());
        ReminderDataBase reminderDataBase = new ReminderDataBase(dataBaseHelper);
        ViewData viewData = reminderDataBase.extractData();

        text = viewData.getText();
        int time = Integer.parseInt(viewData.getTime()) * UntilConstants.MIN;

        timer = new Timer();
        timer.schedule(new ReminderTimerTask(), time, time);
        return super.onStartCommand(intent, flags, startId);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }

    @Nullable
    public IBinder onBind(Intent intent) {
        return null;
    }

    class ReminderTimerTask extends TimerTask{
        private static final String KEY_TEXT_REPLY = "key_text_reply";

        @Override
        public void run() {

            Intent notificationIntent = new Intent(getApplicationContext(), ReminderActivity.class);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),0,notificationIntent,0);

            String replyLabel = getString(R.string.reply_label);
            RemoteInput remoteInput = new RemoteInput.Builder(KEY_TEXT_REPLY)
                    .setLabel(replyLabel)
                    .build();

            Intent replyIntent = new Intent(getApplicationContext(), ReplyReceiver.class);
            PendingIntent replyPendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, replyIntent,0);

            NotificationCompat.Action action =
                    new NotificationCompat.Action.Builder(R.drawable.ic_launcher_foreground, replyLabel, replyPendingIntent)
                            .addRemoteInput(remoteInput)
                            .setAllowGeneratedReplies(true)
                            .build();

            NotificationCompat.Builder notification =
                    new NotificationCompat.Builder(getApplicationContext(), UntilConstants.SERVICE_CHANNEL_ID)
                            .setContentText(text)
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setAutoCancel (true)
                            .setDefaults(NotificationCompat.DEFAULT_SOUND)
                            .setContentIntent(pendingIntent)
                            .addAction(action);

            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());
            notificationManager.notify(UntilConstants.NOTIFY_ID, notification.build());
        }
    }
}
