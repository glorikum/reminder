package com.oshovenko.reminder.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "reminderDataBase";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_TABLE_NAME = "reminderTable";

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + DATABASE_TABLE_NAME + " ("
                + "id integer primary key autoincrement,"
                + "text text,"
                + "time text" + ");");

        ContentValues contentValues = new ContentValues();
        contentValues.put("text", "");
        contentValues.put("time", "");

        db.insert(DATABASE_TABLE_NAME, null, contentValues);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public String getName(){
        return DATABASE_NAME;
    }

    public int getVersion(){
        return DATABASE_VERSION;
    }

    public String getTableName(){
        return DATABASE_TABLE_NAME;
    }
}
