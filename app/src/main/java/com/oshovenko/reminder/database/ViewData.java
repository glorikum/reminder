package com.oshovenko.reminder.database;

/**
 * Class for storing input field values
 */
public class ViewData {
    //Reminder Text
    private String text;

    //Snooze time
    private String time;

    public void setText(String text){
        this.text = text;
    }

    public void setTime(String time){
        this.time = time;
    }

    public String getText(){
        return text;
    }

    public String getTime(){
        return time;
    }
}
