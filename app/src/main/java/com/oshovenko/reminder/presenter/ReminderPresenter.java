package com.oshovenko.reminder.presenter;

import android.app.ActivityManager;
import android.content.Intent;
import com.oshovenko.reminder.constants.UntilConstants;
import com.oshovenko.reminder.model.ReminderDataBase;
import com.oshovenko.reminder.services.ReminderService;
import com.oshovenko.reminder.view.ReminderActivity;
import com.oshovenko.reminder.database.ViewData;

import static android.content.Context.ACTIVITY_SERVICE;


public class ReminderPresenter {
    private ReminderActivity mainActivity;
    private ReminderDataBase reminderDataBase;

    public ReminderPresenter(ReminderDataBase reminderDataBase) {
        this.reminderDataBase = reminderDataBase;
    }

    public void setPreviousValues() {
        mainActivity.setValue(reminderDataBase.extractData());
        mainActivity.setButtonEnable(isServiceRunning());
    }

    private boolean isServiceRunning() {
        ActivityManager manager = (ActivityManager) mainActivity.getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (UntilConstants.SERVICE_NAME.equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void startService() {
            ViewData viewData = mainActivity.getValue();
            if(isTextCorrect(viewData.getText())){
                if (isTimeCorrect(viewData.getTime())){
                    reminderDataBase.loadData(viewData);
                    Intent intent = new Intent(mainActivity.getApplicationContext(), ReminderService.class);
                    mainActivity.startService(intent);
                } else{
                    mainActivity.setTimeError();
                }
            } else {
                mainActivity.setTextError();
            }
    }

    public void stopService() {
        Intent intent = new Intent(mainActivity.getApplicationContext(), ReminderService.class);
        mainActivity.stopService(intent);
    }

    private boolean isTextCorrect(String text) {
        return text.length() < UntilConstants.CORRECT_TEXT_LENGTH;
    }

    private boolean isTimeCorrect(String time) {
        return time.isEmpty();
    }
    
    public void attachView(ReminderActivity reminderActivity) {
        this.mainActivity = reminderActivity;
    }

    public void detachView() {
        mainActivity = null;
    }

    public void updateService(){
        if (isServiceRunning()){
            stopService();
            startService();
        }
    }
}
